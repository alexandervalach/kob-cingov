<?php

namespace App\Model;

/**
 * Description of MembersPresenter
 *
 * @author jonsnow
 */
class MembersRepository extends Repository {
    public static $sex = [
        'M' => ' Muž',
        'W' => ' Žena'
    ];
}
