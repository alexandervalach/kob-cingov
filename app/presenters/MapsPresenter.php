<?php

namespace App\Presenters;

use App\FormHelper;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Nette\Utils\Image;
use Nette\Utils\FileSystem;

class MapsPresenter extends BasePresenter {

    /** @var ActiveRow */
    private $mapRow;

    /** @var ActiveRow */
    private $collectionRow;

    /** @var string */
    private $error = 'Map not found!';

    public function actionAdd($id) {
        $this->userIsLogged();
        $this->collectionRow = $this->mapsCollectionRepository->findById($id);
    }

    public function renderAdd($id) {
        if (!$this->collectionRow) {
            throw new BadRequestException('Map collection not found!');
        }
        $this->template->collection = $this->collectionRow;
        $this->getComponent('uploadForm');
    }

    public function actionRemove($id) {
        $this->userIsLogged();
        $this->mapRow = $this->mapsRepository->findById($id);
    }

    public function renderRemove($id) {
        if (!$this->mapRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->map = $this->mapRow;
        $this->getComponent('removeForm');
    }

    public function actionThumbnail($id) {
        $this->userIsLogged();
        $this->mapRow = $this->mapsRepository->findById($id);
    }

    public function renderThumbnail($id) {
        if (!$this->mapRow) {
            throw new BadRequestException($this->error);
        }
        $this->collectionRow = $this->mapsCollectionRepository->findById($this->mapRow->collection_id);
        $this->template->map = $this->mapRow;
        $this->getComponent('thumbnailForm');
    }

    protected function createComponentUploadForm() {
        $form = new Form;

        $form->addUpload('maps', 'Nahrať mapy', TRUE);
        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;
        $form->addSubmit('upload', 'Nahrať')
                ->onClick[] = $this->submittedUploadForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentRemoveForm() {
        $form = new Form;

        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;
        $form->addSubmit('remove', 'Odstrániť')
                        ->setAttribute('class', 'btn btn-danger')
                ->onClick[] = $this->submittedRemoveForm;

        return $form;
    }

    protected function createComponentThumbnailForm() {
        $this->submittedThumbnailForm();
    }

    public function submittedUploadForm(SubmitButton $btn) {
        $values = $btn->form->getValues();
        $mapData = array();

        foreach ($values['maps'] as $map) {
            $name = strtolower($map->getSanitizedName());

            if ($map->isOk() AND $map->isImage()) {
                $mapPath = $this->imgFolder . '/' . $name;
                $map->move($mapPath);

                $file = Image::fromFile($mapPath);
                $width = $file->getWidth();
                $height = $file->getHeight();

                $mapData = array(
                    'name' => $name,
                    'collection_id' => $this->collectionRow->id,
                    'width' => $width,
                    'height' => $height
                );
                $this->mapsRepository->insert($mapData);
            }
        }
        $this->redirect('MapsCollection:view#page-nav', $this->collectionRow);
    }

    public function submittedRemoveForm() {
        $file = new FileSystem;
        $file->delete($this->imgFolder . '/' . $this->mapRow->name);

        $id = $this->mapRow->collection_id;
        $this->mapRow->delete();

        $this->flashMessage('Mapa ' . $this->mapRow->name . ' bola odstránená.', 'alert-success');
        $this->redirect('MapsCollection:view#page-nav', $id);
    }

    public function submittedThumbnailForm() {
        $data = array('thumbnail' => $this->mapRow->name);
        $this->collectionRow->update($data);
        $this->flashMessage('Miniatúra bola zmenená.', 'alert-success');
        $this->redirect('MapsCollection:view#page-nav', $this->mapRow->collection_id);
    }

    public function formCancelled() {
        $this->redirect('MapsCollection:view#page-nav', $this->mapRow->collection_id);
    }

}
