<?php

namespace App\Presenters;

use App\FormHelper;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Nette\Utils\FileSystem;

class MapsCollectionPresenter extends BasePresenter {

    /** @var ActiveRow */
    private $collectionRow;

    /** @var string */
    private $error = "Maps not found!";

    public function renderAll() {
        $this->template->collection = $this->mapsCollectionRepository->findAll();
        $this->template->imgFolder = $this->imgFolder;
    }

    public function actionAdd() {
        $this->userIsLogged();
    }

    public function renderAdd() {
        $this->getComponent('addForm');
    }

    public function actionView($id) {
        $this->collectionRow = $this->mapsCollectionRepository->findById($id);
    }

    public function renderView($id) {
        if (!$this->collectionRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->collection = $this->collectionRow;
        $this->template->imgFolder = $this->imgFolder;
        $this->template->portrait = $this->mapsRepository->findByValue('collection_id', $id)->where('width >= height');
        $this->template->landscape = $this->mapsRepository->findByValue('collection_id', $id)->where('width < height');
    }

    public function actionEdit($id) {
        $this->userIsLogged();
        $this->collectionRow = $this->mapsCollectionRepository->findById($id);
    }

    public function renderEdit($id) {
        if (!$this->collectionRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->collection = $this->collectionRow;
        $this->getComponent('editForm')->setDefaults($this->collectionRow);
    }

    public function actionRemove($id) {
        $this->userIsLogged();
        $this->collectionRow = $this->mapsCollectionRepository->findById($id);
    }

    public function renderRemove($id) {
        if (!$this->collectionRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->collection = $this->collectionRow;
        $this->getComponent('removeForm');
    }

    protected function createComponentAddForm() {
        $form = new Form();

        $form->addText('name', 'Názov kolekcie')
                ->addRule(Form::FILLED, 'Prosím, vyplňte NÁZOV.');
        $form->addSubmit('save', 'Uložiť');

        $form->onSuccess[] = $this->submittedAddForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentEditForm() {
        $form = new Form();

        $form->addText('name', 'Názov kolekcie')
                ->addRule(Form::FILLED, 'Prosím, vyplňte NÁZOV.');
        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;

        $form->addSubmit('save', 'Uložiť')
                ->onClick[] = $this->submittedEditForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentRemoveForm() {
        $form = new Form;

        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;

        $form->addSubmit('remove', 'Odstrániť')
                        ->setAttribute('class', 'btn btn-danger')
                ->onClick[] = $this->submittedRemoveForm;

        return $form;
    }

    public function submittedAddForm(Form $form) {
        $values = $form->getValues();
        $this->mapsCollectionRepository->insert($values);
        $this->redirect('all#page-nav');
    }

    public function submittedEditForm(SubmitButton $btn) {
        $values = $btn->form->getValues();
        $this->collectionRow->update($values);
        $this->redirect('all#page-nav');
    }

    public function submittedRemoveForm() {
        $this->flashMessage('Kolekcia máp ' . $this->collectionRow->name . ' bola odstránená.', 'alert-success');

        $mapsCollection = $this->mapsRepository->findByValue('collection_id', $this->collectionRow);

        if ($mapsCollection != null) {
            foreach ($mapsCollection as $map) {
                if ($map->thumbnail != '') {
                    $file = new FileSystem;
                    $file->delete($this->imgFolder . '/' . $map->name);
                    $map->delete();
                }
            }
        }

        $this->collectionRow->delete();
        $this->redirect('all#page-nav');
    }

    public function formCancelled() {
        $this->redirect('all#page-nav');
    }

}
