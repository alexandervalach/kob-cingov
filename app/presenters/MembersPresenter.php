<?php

namespace App\Presenters;

use App\FormHelper;
use App\Model\MembersRepository;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;

// TODO Doplniť Date Helper

class MembersPresenter extends BasePresenter {

    /** @var ActiveRow */
    private $memberRow;

    /** @var string */
    private $error = 'Member not found!';

    public function renderAll() {
        $this->template->members = $this->membersRepository->findAll();
    }

    public function actionAdd() {
        $this->userIsLogged();
    }

    public function renderAdd() {
        $this->getComponent('addForm');
    }

    public function actionEdit($id) {
        $this->userIsLogged();
        $this->memberRow = $this->membersRepository->findById($id);
    }

    public function renderEdit($id) {
        if (!$this->memberRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->member = $this->memberRow;
        $this->getComponent('editForm')->setDefaults($this->memberRow);
    }

    public function actionRemove($id) {
        $this->userIsLogged();
        $this->memberRow = $this->membersRepository->findById($id);
    }

    public function renderRemove($id) {
        if (!$this->memberRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->member = $this->memberRow;
        $this->getComponent('removeForm');
    }

    protected function createComponentAddForm() {
        $form = new Form();
        $form->addText('name', 'Meno a priezvisko')
                ->addRule(Form::FILLED, 'Prosím, zadajte meno.');
        $form->addText('regNum', 'Registračné číslo');
        $form->addRadioList('gender', '', MembersRepository::$sex);
        $form->addText('dateOfBirth', 'Dátum narodenia');
        $form->addText('si', 'SI číslo');
        $form->addText('email', 'E-mail')
                ->setType('email');
        $form->addText('phoneNum', 'Telefónne číslo');
        $form->addSubmit('save', 'Uložiť');

        $form->onSuccess[] = $this->submittedAddForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentEditForm() {
        $form = new Form();
        $form->addText('name', 'Meno a priezvisko')
                ->addRule(Form::FILLED, 'Prosím, zadajte meno.');
        $form->addText('regNum', 'Registračné číslo');
        $form->addRadioList('gender', '', MembersRepository::$sex);
        $form->addText('dateOfBirth', 'Dátum narodenia');
        $form->addText('si', 'SI číslo');
        $form->addText('email', 'E-mail')
                ->setType('email');
        $form->addText('phoneNum', 'Telefónne číslo');
        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;
        $form->addSubmit('save', 'Uložiť')
                ->onClick[] = $this->submittedEditForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentRemoveForm() {
        $form = new Form();

        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;

        $form->addSubmit('remove', 'Odstrániť')
                        ->setAttribute('class', 'btn btn-danger')
                ->onClick[] = $this->submittedRemoveForm;

        return $form;
    }

    public function submittedAddForm(Form $form) {
        $values = $form->getValues();
        $this->membersRepository->insert($values);
        $this->redirect('all#page-nav');
    }

    public function submittedEditForm(SubmitButton $btn) {
        $values = $btn->form->getValues();
        $this->memberRow->update($values);
        $this->redirect('all#page-nav');
    }

    public function submittedRemoveForm() {
        $this->flashMessage('Člen \'' . $this->memberRow->name . '\' odstránený z kontaktov.', 'alert-success');
        $this->memberRow->delete();
        $this->redirect('all#page-nav');
    }

    public function formCancelled() {
        $this->redirect('all#page-nav');
    }

}
