<?php

namespace App\Presenters;

use App\Components\ModalDialog;
use App\Model\AlbumsRepository;
use App\Model\LinksRepository;
use App\Model\MapsCollectionRepository;
use App\Model\MapsRepository;
use App\Model\MembersRepository;
use App\Model\ParticipantsRepository;
use App\Model\PhotosRepository;
use App\Model\PostImagesRepository;
use App\Model\PostsRepository;
use App\Model\RacesRepository;
use App\Model\UsersRepository;
use Nette\Application\UI\Presenter;

class BasePresenter extends Presenter {

    /** @var AlbumsRepository */
    protected $albumsRepository;

    /** @var LinksRepository */
    protected $linksRepository;

    /** @var MapsCollectionRepository */
    protected $mapsCollectionRepository;
    
    /** @var MapsRepository */
    protected $mapsRepository;

    /** @var MembersRepository */
    protected $membersRepository;

    /** @var ParticipantsRepository */
    protected $participantsRepository;

    /** @var PhotosRepository */
    protected $photosRepository;

    /** @var PostImagesRepository */
    protected $postImagesRepository;

    /** @var PostsRepository */
    protected $postsRepository;

    /** @var RacesRepository */
    protected $racesRepository;

    /** @var UserRepository */
    protected $usersRepository;

    /** @var string */
    protected $imgFolder = "imgs";

    /** @var string */
    protected $fileFolder = "files";
        
    public function __construct(AlbumsRepository $albumsRepository, LinksRepository $linksRepository, MapsCollectionRepository $mapsCollectionRepository, MapsRepository $mapsRepository, MembersRepository $membersRepository, ParticipantsRepository $participantsRepository, PhotosRepository $photosRepository, PostImagesRepository $postImagesRepository, PostsRepository $postsRepository, RacesRepository $racesRepository, UsersRepository $usersRepository
    ) {
        $this->albumsRepository = $albumsRepository;
        $this->linksRepository = $linksRepository;
        $this->mapsCollectionRepository = $mapsCollectionRepository;
        $this->mapsRepository = $mapsRepository;
        $this->membersRepository = $membersRepository;
        $this->participantsRepository = $participantsRepository;
        $this->photosRepository = $photosRepository;
        $this->postImagesRepository = $postImagesRepository;
        $this->postsRepository = $postsRepository;
        $this->racesRepository = $racesRepository;
        $this->usersRepository = $usersRepository;
    }

    protected function userIsLogged() {
        if (!$this->user->loggedIn) {
            $this->redirect('Sign:in');
        }
    }

    protected function createComponentModalDialog() {
        return new ModalDialog();
    }
}
