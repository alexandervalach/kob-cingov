<?php

namespace App\Presenters;

use App\FormHelper;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;

class PostsPresenter extends BasePresenter {

    /** @var ActiveRow */
    private $postRow;

    /** @var String */
    private $error = "Post not found!";

    public function renderAll() {
        $this->template->posts = $this->postsRepository->findByValue('actual', 1);
    }

    public function renderArchive() {
        $this->template->archives = $this->postsRepository->findByValue('actual', 0);
    }

    public function actionView($id) {
        $this->postRow = $this->postsRepository->findById($id);
    }

    public function renderView($id) {
        if (!$this->postRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->post = $this->postRow;
    }

    public function actionAdd() {
        $this->userIsLogged();
    }

    public function renderAdd() {
        $this->getComponent('addForm');
    }

    public function actionEdit($id) {
        $this->userIsLogged();
        $this->postRow = $this->postsRepository->findById($id);
    }

    public function renderEdit($id) {
        if (!$this->postRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->post = $this->postRow;
        $this->getComponent('editForm')->setDefaults($this->postRow);
    }

    protected function createComponentAddForm() {
        $form = new Form;
        $form->addText('title', 'Názov')
                ->addRule(Form::FILLED, 'Vyplňte, prosím, názov');
        $form->addTextArea('content', 'Článok')
                ->setAttribute('class', 'jqte');
        $form->addText('author', 'Autor');
        $form->addCheckbox('actual', ' Aktuálny')
                ->setDefaultValue('checked');
        $form->addSubmit('save', 'Uložiť');

        $form->onSuccess[] = $this->submittedAddForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentEditForm() {
        $form = new Form;
        $form->addText('title', 'Názov')
                ->addRule(Form::FILLED, 'Vyplňte, prosím názov');
        $form->addTextArea('content', 'Článok')
                ->setAttribute('jqte');
        $form->addText('author', 'Autor');
        $form->addCheckbox('actual', ' Aktuálny');
        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;
        $form->addSubmit('save', 'Uložiť')
                ->onClick[] = $this->submittedEditForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    public function submittedAddForm(Form $form) {
        $values = $form->getValues();
        $postId = $this->postsRepository->insert($values);
        $this->redirect('view#page-nav', $postId);
    }

    public function submittedEditForm(SubmitButton $btn) {
        $values = $btn->form->getValues();
        $this->postRow->update($values);
        $this->redirect('view#page-nav', $this->postRow);
    }
    
    public function submittedRemoveForm() {
        $this->postRow->delete();
        $this->flashMessage('Článok bol odstránený', 'alert-success');
        $this->redirect('all#page-nav');
    }

    public function formCancelled() {
        $this->redirect('view#page-nav', $this->postRow);
    }

}
