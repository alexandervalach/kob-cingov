<?php

namespace App\Presenters;

use App\FormHelper;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Nette\Utils\Image;
use Nette\Utils\FileSystem;

class PhotosPresenter extends BasePresenter {

    /** @var ActiveRow */
    private $photoRow;

    /** @var ActiveRow */
    private $albumRow;

    /** @var string */
    private $error = "Album not found";

    public function actionAdd($id) {
        $this->userIsLogged();
        $this->albumRow = $this->albumsRepository->findById($id);
    }

    public function renderAdd() {
        if (!$this->albumRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->album = $this->albumRow;
        $this->getComponent('uploadForm');
    }

    public function actionThumbnail($id) {
        $this->userIsLogged();
        $this->photoRow = $this->photosRepository->findById($id);
    }

    public function renderThumbnail($id) {
        if (!$this->photoRow) {
            throw new BadRequestException($this->error);
        }
        $this->albumRow = $this->albumsRepository->findById($this->photoRow->album_id);
        $this->template->photo = $this->photoRow;
        $this->getComponent('thumbnailForm');
    }

    public function actionRemove($id) {
        $this->userIsLogged();
        $this->photoRow = $this->photosRepository->findById($id);
    }

    public function renderRemove($id) {
        if (!$this->photoRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->photo = $this->photoRow;
        $this->getComponent('removeForm');
    }

    public function actionCut($id) {
        $this->userIsLogged();
        $this->photoRow = $this->photosRepository->findById($id);
    }

    public function renderCut($id) {
        
    }

    protected function createComponentUploadForm() {
        $form = new Form;
        $form->addUpload('photos', 'Fotografie: ', TRUE);
        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;
        $form->addSubmit('save', 'Nahrať')
                ->onClick[] = $this->submittedUploadForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentRemoveForm() {
        $form = new Form();

        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;

        $form->addSubmit('remove', 'Odstrániť')
                        ->setAttribute('class', 'btn btn-danger')
                ->onClick[] = $this->submittedRemoveForm;

        return $form;
    }

    protected function createComponentThumbnailForm() {
        $this->submittedThumbnailForm();
    }

    public function submittedUploadForm(SubmitButton $btn) {
        $values = $btn->form->getValues();
        $imgData = array();

        foreach ($values['photos'] as $img) {
            $name = strtolower($img->getSanitizedName());

            if ($img->isOk() AND $img->isImage()) {
                $imgPath = $this->imgFolder . '/' . $name;
                $img->move($imgPath);

                $image = Image::fromFile($imgPath);
                $height = $image->getHeight();
                $width = $image->getWidth();

                $imgData = array(
                    'name' => $name,
                    'album_id' => $this->albumRow,
                    'width' => $width,
                    'height' => $height
                );
                $this->photosRepository->insert($imgData);
            }
        }
        $this->redirect('Albums:view#page-nav', $this->albumRow);
    }

    public function submittedRemoveForm() {
        $file = new FileSystem;
        $file->delete($this->imgFolder . '/' . $this->photoRow->name);

        $id = $this->photoRow->album_id;
        $this->photoRow->delete();

        $this->flashMessage('Obrázok bol odstránený.', 'alert-success');
        $this->redirect('Albums:view#page-nav', $id);
    }

    public function submittedThumbnailForm() {
        $data = array('thumbnail' => $this->photoRow->name);
        $this->albumRow->update($data);
        $this->flashMessage('Miniatúra bola zmenená.', 'alert-success');
        $this->redirect('Albums:view#page-nav', $this->photoRow->album_id);
    }

    public function formCancelled() {
        $this->redirect('Albums:view#page-nav', $this->photoRow->album_id);
    }

}
