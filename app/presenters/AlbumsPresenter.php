<?php

namespace App\Presenters;

use App\FormHelper;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Nette\Utils\FileSystem;

class AlbumsPresenter extends BasePresenter {

    /** @var ActiveRow */
    private $albumRow;

    /** @var string */
    private $error = "Album not found!";

    public function renderAll() {        
        $this->template->albums = $this->albumsRepository->findAll();
        $this->template->imgFolder = $this->imgFolder;
    }

    public function actionView($id) {
        $this->albumRow = $this->albumsRepository->findById($id);
    }

    public function renderView($id) {
        if (!$this->albumRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->portrait = $this->photosRepository->findByValue('album_id', $id)->where('width >= height');
        $this->template->landscape = $this->photosRepository->findByValue('album_id', $id)->where('width < height');
        $this->template->album = $this->albumRow;
        $this->template->imgFolder = $this->imgFolder;
    }

    public function actionAdd() {
        $this->userIsLogged();
    }

    public function renderAdd() {
        $this->getComponent('addForm');
    }

    public function actionEdit($id) {
        $this->userIsLogged();
        $this->albumRow = $this->albumsRepository->findById($id);
    }

    public function renderEdit($id) {
        if (!$this->albumRow) {
            throw new BadRequestException($this->error);
        }
        $this->getComponent('editForm')->setDefaults($this->albumRow);
    }

    public function actionRemove($id) {
        $this->userIsLogged();
        $this->albumRow = $this->albumsRepository->findById($id);
    }

    public function renderRemove($id) {
        if (!$this->albumRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->album = $this->albumRow;
        $this->getComponent('removeForm');
    }

    protected function createComponentAddForm() {
        $form = new Form;

        $form->addText('name', 'Názov')->addRule(Form::FILLED, 'Prosím, vyplňte meno');
        $form->addSubmit('save', 'Uložiť');

        $form->onSuccess[] = $this->submittedAddForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentEditForm() {
        $form = new Form;

        $form->addText('name', 'Názov')->addRule(Form::FILLED, 'Prosím, vyplňte meno');
        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;
        $form->addSubmit('save', 'Uložiť')
                ->onClick[] = $this->submittedEditForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentRemoveForm() {
        $form = new Form();

        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;

        $form->addSubmit('remove', 'Odstrániť')
                        ->setAttribute('class', 'btn btn-danger')
                ->onClick[] = $this->submittedRemoveForm;

        return $form;
    }

    public function submittedAddForm(Form $form) {
        $values = $form->getValues();
        $this->albumsRepository->insert($values);
        $this->redirect('all#page-nav');
    }

    public function submittedEditForm(SubmitButton $btn) {
        $values = $btn->form->getValues();
        $this->albumRow->update($values);
        $this->redirect('all#page-nav');
    }

    public function submittedRemoveForm() {
        $this->flashMessage('Album ' . $this->albumRow->name . ' bol odstránený.', 'alert-success');

        $albumPhotos = $this->photosRepository->findByValue('album_id', $this->albumRow);

        if ($albumPhotos != null) {
            foreach ($albumPhotos as $photo) {
                if ($photo->thumbnail != '') {
                    $file = new FileSystem;
                    $file->delete($this->imgFolder . '/' . $photo->name);
                    $photo->delete();
                }
            }
        }

        $this->albumRow->delete();
        $this->redirect('all#page-nav');
    }

    public function formCancelled() {
        $this->redirect('all#page-nav');
    }

}
