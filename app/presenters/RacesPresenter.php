<?php

namespace App\Presenters;

use App\FormHelper;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;

class RacesPresenter extends BasePresenter {

    /** @var ActiveRow */
    private $raceRow;

    /** @var string */
    private $error = "Race not found!";

    public function renderAll() {
        $this->template->races = $this->racesRepository->findAll();
    }

    public function actionAdd() {
        $this->userIsLogged();
    }

    public function renderAdd() {
        $this->getComponent('addForm');
    }

    public function actionEdit($id) {
        $this->userIsLogged();
        $this->raceRow = $this->racesRepository->findById($id);
    }

    public function renderEdit($id) {
        if (!$this->raceRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->race = $this->raceRow;
        $this->getComponent('editForm')->setDefaults($this->raceRow);
    }
    
    public function actionRemove($id) {
        $this->userIsLogged();
        $this->raceRow = $this->racesRepository->findById($id);
    }
    
    public function renderRemove($id) {
        if (!$this->raceRow) {
            throw new BadRequestException($this->error);
        }
        $this->template->race = $this->raceRow;
        $this->getComponent('removeForm');
    }

    protected function createComponentAddForm() {
        $form = new Form;
        $form->addText('name', 'Názov')
                ->addRule(Form::FILLED, 'Prosím, vyplňte NÁZOV.');
        $form->addText('dateFrom', 'Dátum začiatku podujatia');
        $form->addText('dateTo', 'Dátum konca podujatia');
        $form->addText('regTime', 'Dátum ukončenia registrácie');
        $form->addText('place', 'Miesto');
        $form->addText('note', 'Poznámka');
        $form->addSubmit('save', 'Uložiť');
               
        $form->onSuccess[] = $this->submittedAddForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentEditForm() {
        $form = new Form;
        $form->addText('name', 'Názov')
                ->addRule(Form::FILLED, 'Prosím, vyplňte NÁZOV.');
        $form->addText('dateFrom', 'Dátum začiatku podujatia');
        $form->addText('dateTo', 'Dátum konca podujatia');
        $form->addText('regTime', 'Dátum ukončenia registrácie');
        $form->addText('place', 'Miesto');
        $form->addText('note', 'Poznámka');
        
        $form->addSubmit('cancel', 'Zrušiť')
                        ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;
        $form->addSubmit('save', 'Uložiť')
                ->onClick[] = $this->submittedEditForm;

        FormHelper::setBootstrapRenderer($form);
        return $form;
    }

    protected function createComponentRemoveForm() {
        $form = new Form();
        
        $form->addSubmit('cancel', 'Zrušiť')
                ->setAttribute('class', 'btn btn-warning')
                ->onClick[] = $this->formCancelled;
        
        $form->addSubmit('remove', 'Odstrániť')
                ->setAttribute('class', 'btn btn-danger')
                ->onClick[] = $this->submittedRemoveForm;
 
        return $form;
    }

    public function submittedAddForm(Form $form) {
        $values = $form->getValues();
        $this->racesRepository->insert($values);
        $this->redirect('all#page-nav');
    }

    public function submittedEditForm(SubmitButton $btn) {
        $values = $btn->form->getValues();
        $this->raceRow->update($values);
        $this->redirect('all#page-nav');
    }

    public function submittedRemoveForm() {
        $this->flashMessage('Preteky \"' . $this->raceRow->name . '\" boli odstránené.', 'alert-success');
        $this->raceRow->delete();
        $this->redirect('all#page-nav');
    }

    public function formCancelled() {
        $this->redirect('all#page-nav');
    }

}
